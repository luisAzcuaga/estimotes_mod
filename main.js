var http = require ('http');
var fs = require('fs');
var express = require('express');
var ejs = require ('ejs');
var noble = require('noble');

var estimotesplugin = require ('./estimotesplugin');

var app = express();
app.set('view engine', 'ejs');

var connectedEstimotes = [{
	id : 'n/a',
	temp : 'n/a',
	acceleration : {x:'n/a',y:'n/a',z:'n/a'},
	isMoving : 'notSure',
	batt : 'n/a',
	motionStateDuration : { current:{number:'n/a',unit:'n/a'},previous:{number:'n/a',unit:'n/a'}}
}];

noble.on('stateChange', function(state) {
	console.log('state has changed', state);
	if (state == 'poweredOn') {
		var allowDuplicates = true;
		noble.startScanning([], allowDuplicates, function(error) {
			if (error) {
				console.log('error starting scanning', error);
			} else {
				console.log('started scanning');
			}
		});
	}
});

noble.on('discover', function(peripheral) {
	var data = peripheral.advertisement.manufacturerData;
	if (!data) { return; }
	if (estimotesplugin.nearableStuff(data)) {
		console.log('--------------')
		var id = estimotesplugin.nearableStuff(data).id;
		var found = false;
		for (var index = 0; index < connectedEstimotes.length; index++) {
			if (connectedEstimotes[index].id == id) {
				console.log('Found ' + id + ' on array!')
				found = true;
				break;
			}
		}
		if (found) {
			connectedEstimotes[index].temp = Math.round((estimotesplugin.nearableStuff(data).temp) * 10) / 10;
			connectedEstimotes[index].acceleration.x = estimotesplugin.nearableStuff(data).acceleration.x;
			connectedEstimotes[index].acceleration.y = estimotesplugin.nearableStuff(data).acceleration.y;
			connectedEstimotes[index].acceleration.z = (estimotesplugin.nearableStuff(data).acceleration.z) + 980;
			connectedEstimotes[index].isMoving = estimotesplugin.nearableStuff(data).isMoving;
			connectedEstimotes[index].batt = Math.round((estimotesplugin.nearableStuff(data).batt) * 10) / 10;
			connectedEstimotes[index].motionStateDuration.current = estimotesplugin.nearableStuff(data).motionStateDuration.current;
			connectedEstimotes[index].motionStateDuration.previous = estimotesplugin.nearableStuff(data).motionStateDuration.previous;
			console.log('Values on ' + id +' updated!')
		} else {
						var newId = estimotesplugin.nearableStuff(data).id;
					if (connectedEstimotes[0].id == 'n/a') { //If the array is empty use that first position
						connectedEstimotes[0] = estimotesplugin.nearableStuff(data);
					} else {
						connectedEstimotes.push(estimotesplugin.nearableStuff(data));
					}
					console.log(newId + ' registered!');
				}

			}
		});

app.get('/', function(req,res) {
	console.log('--------------')
	res.render('index',{
		connectedEstimotes : connectedEstimotes
	});
	console.log(connectedEstimotes);
})

app.listen(3000);

console.log('Now Listening on port 3000');