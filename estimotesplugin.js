function nearableStuff(data) {
	var companyId = data.readUInt16LE(0);
	if (companyId != 0x015d) { return; }
	var frameType = data.readUInt8(2);
	if (frameType != 0x01) { return; }

	// ESTIMOTE ID
	if (data.toString('hex', 3, 11) === 'e70c765b26718f87') {
		var id = 'Bicla';	
	}
	else if (data.toString('hex', 3, 11) === '08949f0516062496'){
		var id = 'Perro';
	}
	else if (data.toString('hex', 3, 11) === 'be034e1cccd585e4'){
		var id = 'Coche';
	}
	else if (data.toString('hex', 3, 11) === '8b300b6c6cb51c0f'){
		var id = 'Silla';
	}
	else if (data.toString('hex', 3, 11) === '0e8e5caf3896180f'){
		var id = 'Puerta';
	}
	else if (data.toString('hex', 3, 11) === '8a7659798e6f3f90'){
		var id = 'Cama';
	}
	else if (data.toString('hex', 3, 11) === 'f7c2c346e6c75b7e'){
		var id = 'Sin Logo';
	}
	else if (data.toString('hex', 3, 11) === '899557159d0389ea'){
		var id = 'Bolso de dama';
	}
	else if (data.toString('hex', 3, 11) === '0061d4cc0fd36020'){
		var id = 'Refrigerador';
	}
	else {
		var id = data.toString('hex', 3, 11);
	}

	// ESTIMOTE TEMPERATURE
	var temperatureRawValue = data.readUInt16LE(13) & 0x0fff
	if (temperatureRawValue > 2047) {
		temperatureRawValue = temperatureRawValue - 4096;
	}
	
	var temp = (temperatureRawValue / 16.0)-3;

	// ESTIMOTE ACCELERATION
	var acceleration = {
		x: data.readInt8(16) * 15.625,
		y: data.readInt8(17) * 15.625,
		z: data.readInt8(18) * 15.625
	};

	// ESTIMOTE ISMOVING?
	var isMoving = (data.readUInt8(15) & 0b01000000) != 0;

	// ESTIMOTE REMAINING_BATT
	var voltageRawValue = ((data.readUInt8(14) & 0b11110000) >> 4)
	| ((data.readUInt8(15) & 0b00111111) << 4);
	var batt = (3 * 1.2 * voltageRawValue) / 1023;

	// TIME SINCE ISMOVING CHANGE
  var parseMotionStateDuration = function(byte) {
    var number = byte & 0b00111111;
    var unitCode = (byte & 0b11000000) >> 6;
    var unit;
    if (unitCode == 0) {
      unit = 'seconds';
    } else if (unitCode == 1) {
      unit = 'minutes';
    } else if (unitCode == 2) {
      unit = 'hours';
    } else if (unitCode == 3 && number < 32) {
      unit = 'days';
    } else {
      unit = 'weeks';
      number = number - 32;
    }
    return {number: number, unit: unit};
  }
  var motionStateDuration = {
    current: parseMotionStateDuration(data.readUInt8(19)),
    previous: parseMotionStateDuration(data.readUInt8(20))
  };

	return {
		id,
		temp,
		acceleration,
		isMoving,
		batt,
		motionStateDuration,
	}
}

module.exports = {
	nearableStuff : nearableStuff,
}