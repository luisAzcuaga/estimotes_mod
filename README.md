# Estimotes

Ya que clonaron el repo en sus computadoras ejecuten

Estamos acostumbrados a que en java debes descargar una librería e importarla en eclipse/arduino/netbeans y luego llamarla.
En node no nos alejamos de esto, sólo que para compartirlo es más "sencillo" al sólo tener que especificar qué necesita nuestro proyecto y almacenarlo en un archivo llamado package.json (no lo toquen), ahí le dice al proyecto qué debe descargar cuando se ejecuta el siguiente comando:

``npm install``

Si después de esto te marca un error, intenta instalando los "prerequisitos de Noble" con el siguiente comando:

``sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev``

hecho esto, repite el primer comando (``npm install``).


Acaban de instalar "Noble" y todo lo que esa dependencia necesita para funcionar, sin embargo, Noble puede que les marque un error al no ser super usuario, para resolverlo ejecutan el siguiente comando:

``sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)``

Ahora sólo falta ejecutar nuestro script que busca los estimotes (nearable) y genera un UI en el navegador. Cabe mencionar que tenemos lo nearables, así que usamos:

``node main.js``


### Questions?

For any questions, post on [Estimote Forums][ef], or email us at <contact@estimote.com>.

[ef]: https://forums.estimote.com/
